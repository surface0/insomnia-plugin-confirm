module.exports.templateTags = [
  {
    displayName: 'Confirm',
    name: 'confirm',
    description: 'show confirmation prompt',
    args: [
      {
        displayName: 'Title',
        type: 'string',
        validate: v => (v ? '' : 'Required'),
      },
      {
        displayName: 'Label',
        type: 'string',
      },
      {
        displayName: 'Confirmation Keyword',
        type: 'string',
        validate: v => (v ? '' : 'Required'),
      },
    ],
    async run(context, title, label, keyword) {
      if (!title) {
        throw new Error('Title attribute is required for prompt tag');
      }

      if (!keyword) {
        throw new Error('Keyword attribute is required for prompt tag');
      }

      const value = await context.app.prompt(title || 'Confirm', {
        label,
        cancelable: true,
        defaultValue: '',
        inputType:'text',
      });

      if (context.renderPurpose === 'send' && keyword !== value) {
        throw new Error('Your input and keyword do not match');
      }

      return keyword;
    },
  },
];
